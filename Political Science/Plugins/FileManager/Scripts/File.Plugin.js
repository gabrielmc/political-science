﻿var fileBrowserSelector = ".file-browser";
$(document).ready(() => {
	updateBrowser(window.location.hash.substr(1), fileBrowserSelector);

	$(window).on("hashchange", () => {
		updateBrowser(window.location.hash.substr(1), fileBrowserSelector);
	});

	$(fileBrowserSelector).on("click", ".delete-file", e => {
		var tableRow = $(e.target).closest('tr');
		var filePath = $(e.target).data("filepath");
		deleteFile(filePath, tableRow);
	});

	$("#file-upload-input").change(() => {
		submitUploadForm(window.location.hash.substr(1));
	});

	$(".create-folder-btn").click(() => {
		createDirectory(window.location.hash.substr(1));
		updateBrowser(window.location.hash.substr(1), fileBrowserSelector);
	});

	$(".up-a-level").click(() => {
		var hash = window.location.hash.substr(1);
		if (hash.length > 0) {
			window.location.hash = hash.substring(0, hash.lastIndexOf("/"));
		}
	});
});

function updateBrowser(path, selector) {
	displayLoadingIndicator();
	$.get(
		"/FileManager/DirectoryContents",
		{ "Path": path },
		response => {
			$(selector).html(response);
			hideLoadingIndicator();
		}).fail(() => {
			hideLoadingIndicator();
			displayErrorDialog();
		});
}

function createDirectory(path) {
	//TODO: This should be pretty.
    var directoryName = prompt("Name for the directory: ");
    if (directoryName) {
        $.post(
            "/FileManager/CreateDirectory",
            { "Path": path, "DirectoryName": directoryName },
            success => {
                if (success) {
                    hideLoadingIndicator();
                } else {
                    hideLoadingIndicator();
                    displayErrorDialog();
                }
                updateBrowser(window.location.hash.substr(1), fileBrowserSelector);
            }
        ).fail(() => {
            hideLoadingIndicator();
            displayErrorDialog();
        });
    }
}

function submitUploadForm(path) {
	console.log(path);
	displayLoadingIndicator();
	var data = new FormData($("#file-upload-form")[0]);

	if (path == null || path.length == 0) {
		path = "/";
	}

	data.append("Path", path);

	$.ajax({
		method: "POST",
		url: "/FileManager/UploadFiles",
		processData: false,
		contentType: false,
		data: data,
		success: success => {
			if (success) {
				hideLoadingIndicator();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
			updateBrowser(window.location.hash.substr(1), fileBrowserSelector);
		},
		error: err => {
			hideLoadingIndicator();
			displayErrorDialog();
			updateBrowser(window.location.hash.substr(1), fileBrowserSelector);
		}
	});

	$("#file-upload-form")[0].reset();
}

function deleteFile(path, parentElement) {
	displayLoadingIndicator();
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/FileManager/DeleteFile",
		{ "Path": path },
		success => {
			if (success) {
				$(parentElement).remove();
				hideLoadingIndicator();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}).fail(() => {
			hideLoadingIndicator();
			displayErrorDialog();
		});
}

function displayDeleteConfirmation() {
	return confirm("Are you sure you'd like to delete this element?");
}

function displayLoadingIndicator() {
	console.warn("Implement loading indicator.");
}

function hideLoadingIndicator() {
	console.warn("Implement loading indicator hide.");
}

function displayErrorDialog() {
	console.warn("Implement error dialog.");
}

function CopyFilePath(path) {
	$("#file-path").select(document.execCommand("copy"));
}