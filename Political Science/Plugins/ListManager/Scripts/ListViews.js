﻿// Create your list View functions here //
// Each function will pass in the context (the html element where data will be displayed) //
// Data is the json of the list //

function loadView(context, data) {

    var htmlStr = "";
    htmlStr = "<div class='faculty-area'>";

    for (var i = 0; i < data.Rows.length; i++) {
        htmlStr += "<a href=''>";
        htmlStr += "<div class='faculty-tile'>";
        htmlStr += "<img class='faculty-img'" + "src='" + data.Rows[i].Image + "'/>";
        htmlStr += "<div class='faculty-name'>" + data.Rows[i].Name + "</div>";
        htmlStr += "<p>" + data.Rows[i].Title + "</p>";
        htmlStr += "<p>" + data.Rows[i].Office + "</p>";
        htmlStr += "<p>" + data.Rows[i].Phone + "</p>";
        htmlStr += "<p>" + data.Rows[i].Email + "</p>";
        htmlStr += "<p>" + data.Rows[i].Email2 + "</p>";
        htmlStr += "<p><em>" + data.Rows[i].Responsibility + "</em></p>";
        htmlStr += "</div>";
        htmlStr += "</a>";
    }

    htmlStr += "</div>";
    $(context).html(htmlStr);

    if (data.Rows[0].Name == "Michael Barber"){
        console.log("we found michael!");
    }
    if (data.Rows[1].Name == "Celeste Beesley") {
        console.log("we found celeste!");
    }

    console.log("help");

}

function SubfieldView(context, data) {

    var htmlStr = "";
    htmlStr = "<div class='faculty-area'>";

    for (var i = 0; i < data.Rows.length; i++) {
        htmlStr += "<a href=''>";
        htmlStr += "<div class='faculty-tile'>";
        htmlStr += "<img class='faculty-img'" + "src='" + data.Rows[i].Image + "'/>";
        htmlStr += "<div class='faculty-name'>" + data.Rows[i].Name + "</div>";
        htmlStr += "<p>" + data.Rows[i].Title + "</p>";
        htmlStr += "<p>" + data.Rows[i].Office + "</p>";
        htmlStr += "<p>" + data.Rows[i].Phone + "</p>";
        htmlStr += "<p>" + data.Rows[i].Email + "</p>";
        htmlStr += "<p>" + data.Rows[i].Email2 + "</p>";
        htmlStr += "<p><em>" + data.Rows[i].Responsibility + "</em></p>";
        htmlStr += "</div>";
        htmlStr += "</a>";
    }

    htmlStr += "</div>";
    $(context).html(htmlStr);

    if (data.Rows[0].Name == "Michael Barber") {
        console.log("we found michael!");
    }
    if (data.Rows[1].Name == "Celeste Beesley") {
        console.log("we found celeste!");
    }

    console.log("help");

}