﻿var listBrowserSelector = ".list-browser";
var listColumnSelector = ".list-columns";
var listItemsSelector = ".list-items";
var listPermissionsSelector = ".list-permissions";
var listNameSelector = ".main-controls";
var listView = ".list-view";
var table = $("#list-items-table");
$(document).ready(() => {
    $('[data-toggle="tooltip"]').tooltip({ container: 'body' });

    updateListBrowser(window.location.hash.substr(1), listBrowserSelector);

    $(window).on("hashchange", () => {
        updateBrowser(window.location.hash.substr(1), listBrowserSelector);
    });

    // Delete list event function //
    $(listBrowserSelector).on("click", ".delete-list", e => {
        var tableRow = $(e.target).closest('tr');
        var filePath = $(e.target).data("listid");
        deleteList(filePath, tableRow);
    });

    // Create list event function //
    $(listBrowserSelector).on("click", ".list-create-input", e => {
        createList();
    });

    // Delete list item event function //
    $(".list-manager").on("click", ".add-list-item", e => {
        var list = $(e.target).data("list-name");
        addListItem(list);
    });

    // Edit list name event function //
    $(listNameSelector).on("focus", ".edit-list-name", e => {
        this.currentVal = $(e.target).val();
    }).on("change", ".edit-list-name", e => {
        var newVal = $(e.target).val();
        editListName(currentVal, newVal);
    });

    // Add column event function //
    $(listColumnSelector).on("click", ".add-column", e => {
        var list = $(e.target).data("list-name");
        AddColumn(list);
    });

    // Delete column event function //
    $(listColumnSelector).on("click", ".delete-column", e => {
        var columnName = $(e.target).data("column-name");
        var tableRow = $(e.target).closest('tr');
        var list = $(e.target).data("list-name");
        removeColumn(list, columnName, tableRow);
    });

    // Edit column type event function //
    $(listColumnSelector).on("focus", ".column-type", e => {
        var currentVal = $(e.target).val();
    }).on("change", ".column-type", e => {
        var newVal = $(e.target).val();
        var columnName = $(e.target).data("column-name");
        var list = $(e.target).data("list-name");
        // Check if column is a new column, if it is a new column without a column name, name must be entered in before editing column type //
        if (columnName == null || columnName == "") {
            alert("Please enter a column name before picking a column type!");
            this.stop();
        }
        else {
            changeColumnType(list, columnName, newVal);
        }
    });

    // edit column name event function //
    $(listColumnSelector).on("focus", ".column-name", e => {
        this.currentVal = $(e.target).val();
    }).on('change', ".column-name", e => {
        this.newVal = $(e.target).val();
        var list = $(e.target).data("list-name");
        if (this.currentVal != "") {
            changeColumnName(list, currentVal, newVal, "");
        }
        else {
            var columnRow = $(e.target).closest('tr');
            changeColumnName(list, "", newVal, columnRow);
        }
    });

    $(listColumnSelector).on("focus", ".column-description", e => {
        var currentVal = $(e.target).val();
    }).on("change", ".column-description", e => {
        var description = $(e.target).val();
        var columnName = $(e.target).data("column-name");
        var list = $(e.target).data("list-name");
        // Check if column is a new column, if it is a new column without a column name, name must be entered in before editing column type //
        if (columnName == null || columnName == "") {
            alert("Please enter a column name before picking a column description!");
            this.stop();
        }
        else {
            changeColumnDescription(list, columnName, description);
        }
    });

    // Edit list item value event function //
    $(listItemsSelector).on("change", ".column-value", e => {
        var newVal = $(e.target).val();
        if ($(e.target).attr("type") == "checkbox") {
            if ($(e.target).is(":checked")) {
                newVal = true;
            }
            else {
                newVal = false;
            }
        }
        var column = $(e.target).data("column-name");
        var id = $(e.target).data("list-item-id");
        var list = $(e.target).data("list-name");
        saveListItemValue(list, id, column, newVal);
    });

    // List permissions edit event //
    $(listPermissionsSelector).on("change", ".permission-value", e => {
        var permission = $(e.target).data("permission-name");
        var newVal = $(e.target).val();
        if ($(e.target).attr("type") == "checkbox") {
            if ($(e.target).is(":checked")) {
                newVal = true;
            }
            else {
                newVal = false;
            }
        }
        var list = $(e.target).data("list-name");
        saveListPermissions(list, permission, newVal);
    });

    // Delete list item event function //
    $(listItemsSelector).on("click", ".list-item-delete", e => {
        var tableRow = $(e.target);
        var id = $(e.target).data("list-item-id");
        var list = $(e.target).data("list-name");
        deleteListItem(list, id, tableRow);
    });

    // Function for searching the list
    $(listNameSelector).on("keyup", ".search-box", e => {
        var data = $(e.target).val().split(" ");
        var table = $("#list-items-table").find("tbody tr");
        if ($(e.target).val() == "") {
            table.show();
            return;
        }

        table.hide();

        table.filter(function (i, v) {
            var $t = $(this);
            for (var d = 0; d < data.length; ++d) {
                if ($t.is(":contains('" + data[d] + "')")) {
                    return true;
                }
            }
            return false;
        }).show();
        $(listItemsSelector).find(".desc, .asc").remove();
    });

    // Function for sorting columns //
    $(listItemsSelector).on("click", ".column-name", e => {
        var column = $(e.target).data("column-name");
        var table = $("#list-items-table");
        var index = 0;
        var th = table.find("th").each(function (i) {
            if (column == $(this).data("column-name")) {
                index = i;
            }
        });
        var dir, switching, shouldSwitch, x, y, switchcount = 0, rows;
        switching = true;
        //Set the sorting direction to ascending:
        dir = "asc";
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.find("TBODY TR");
            /*Loop through all table rows (except the
            first, which contains table headers):*/
            for (i = 0; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[index];
                y = rows[i + 1].getElementsByTagName("TD")[index];
                /*check if the two rows should switch place,
                based on the direction, asc or desc:*/
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                //Each time a switch is done, increase this count by 1:
                switchcount++;
            } else {
                /*If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again.*/
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
        if (dir == "asc") {
            $(e.target).find(".desc, .asc").remove();
            $(e.target).parent().find(".desc, .asc").remove();
            $(e.target).append("<span class='glyphicon glyphicon-sort-by-alphabet asc' style='padding-left: 5px;'></span>");
        }
        else {
            $(e.target).find(".desc, .asc").remove();
            $(e.target).parent().find(".desc, .asc").remove();
            $(e.target).append("<span class='glyphicon glyphicon-sort-by-alphabet-alt desc' style='padding-left: 5px;'></span>");
        }
    });

    $(listItemsSelector).on("click", ".save-item", e => {
        alert("Your changes have been saved.");
    });

    if ($(listView)[0]) {
        var script = document.createElement("script");
        script.src = "/Plugins/ListManager/Scripts/ListViews.js";
        script.type = "text/javascript";
        document.body.append(script);
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange == null;
                    var list = $(listView).data("list-name");
                    var method = $(listView).data("list-method");
                    var data = listService(list, method, $(listView));
                }
            }
        }
        else {
            script.onload = function () {
                var list = $(listView).data("list-name");
                    var method = $(listView).data("list-method");
                    var data = listService(list, method, $(listView));
            }
        }
    }
});

var listCallbackElement;
function listCallback(fileName) {
    console.log(fileName);
    $("#file-picker-modal").modal("hide");
    listCallbackElement.val(fileName);
    var column = listCallbackElement.data("column-name");
    var id = listCallbackElement.data("list-item-id");
    var newVal = listCallbackElement.val();
    var list = $('.list-name').html();
    saveListItemValue(list, id, column, newVal);
}

function filePickerCallBack(element, meta) {
    listCallbackElement = $(element).parent().parent().find("input")[0];
    listCallbackElement = $(listCallbackElement);
    $.get("/FileManager/FilePicker?Target=listCallback&FileType=" + meta.filetype.toString(),
        filePicker => {
            $("body").append(filePicker);
            $("#file-picker-modal").modal("show");
        }
    );
}

// Function for service to call a list item //
function listItemService(listName, itemId) {
    var list = listName;
    var itemid = itemId;
    $.get(
        "/ListManager/GetListItem/?listName=" + list + "&id=" + itemid,
        success => {
            if (success) {
                var data = JSON.parse(success);
                hideLoadingIndicator();
                console.log(data);
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
                console.log(success);
            }
        }
    )
        .fail(function (jqXHR, textStatus, errorThrown) {
            hideLoadingIndicator();
            displayErrorDialog();
        });
}

// Function for service to call a list //
function listService(list, callback, context) {
    $.get(
        "/ListManager/GetList/" + list,
        success => {
            if (success) {
                var data = JSON.parse(success);
                hideLoadingIndicator();
                console.log(data);
                window[callback](context, data);
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
                console.log(success);
            }
        }
    )
        .fail(function (jqXHR, textStatus, errorThrown) {
            hideLoadingIndicator();
            displayErrorDialog();
        });
}

// Function to create a list //
function createList() {
	// Prompt for name of list //
	var listName = prompt("What is the name of your new list?");
	if (listName != null) {
		$.post(
			"/ListManager/AddList",
			{ "id": listName },
			success => {
				if (success) {
					hideLoadingIndicator();
					// Load the edit list form for the new list created //
					window.location.href = "/ListManager/EditList/" + listName;
				} else {
					hideLoadingIndicator();
					displayErrorDialog();
				}
			}
		).fail(function (jqXHR, textStatus, errorThrown) {
			hideLoadingIndicator();
			displayErrorDialog();
		});
    }
    else {
        alert("Error: Please enter a valid list name!");
    }
}

// Function to edit list permissoins //
function saveListPermissions(list, permission, value) {
    $.post(
        "/ListManager/EditListPermissions",
        { "list": list, "permission": permission, "value": value },
        success => {
            if (success) {
                hideLoadingIndicator();
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
            }
        }
    ).fail(function (jqXHR, textStatus, errorThrown) {
        hideLoadingIndicator();
        displayErrorDialog();
    });
}

// Function to save a list item //
function saveListItemValue(list, id, column, value) {
	$.post(
		"/ListManager/SaveListItem",
		{ "list": list, "id": id, "column": column, "value": value },
		success => {
			if (success) {
				hideLoadingIndicator();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

// Function to delete a list item //
function deleteListItem(list, id, tableRow) {
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/ListManager/DeleteListItem",
		{ "list": list, "id": id },
		success => {
			if (success) {
				hideLoadingIndicator();
				tableRow.parent().parent().remove();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function to add a list item //
function addListItem(list) {
	$.post(
		"/ListManager/AddListItemID",
		{ "list": list },
		success => {
			console.log(success);
			if (success > -1) {
				hideLoadingIndicator();
				window.location.href = "/ListManager/AddListItem/" + list + "?id=" + success;
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function for editing list name //
function editListName(currentVal, newVal) {
	$.post(
		"/ListManager/EditListName",
		{ "oldName": currentVal, "newName": newVal },
		success => {
			if (success) {
				hideLoadingIndicator();
				window.location.href = "/ListManager/EditList/" + newVal;
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function for editing a column name //
function changeColumnName(list, currentVal, newVal, columnRow) {
	// Check to see if column is a new column //
	if (currentVal != "") {
		$.post(
			"/ListManager/PutListColumnName",
            { "id": list, "oldColumn": currentVal, "newColumn": newVal },
			success => {
				if (success) {
					hideLoadingIndicator();
				} else {
					hideLoadingIndicator();
					displayErrorDialog();
				}
			}
		).fail(function (jqXHR, textStatus, errorThrown) {
			hideLoadingIndicator();
			displayErrorDialog();
		});
	}
	else {
		$.post(
			"/ListManager/AddColumn",
            { "id": list, "column": newVal },
			success => {
				if (success) {
					columnRow.find("select").attr("data-column-name", newVal);
					columnRow.find("button").attr("data-column-name", newVal);
                    columnRow.find("input").attr("data-column-name", newVal);
					hideLoadingIndicator();
				} else {
					hideLoadingIndicator();
					displayErrorDialog();
				}
			}
		).fail(function (jqXHR, textStatus, errorThrown) {
			hideLoadingIndicator();
			displayErrorDialog();
		});
	}
}

// Function for changing a column type //
function changeColumnType(list, columnName, newType) {
	displayLoadingIndicator();
	if (!displayColumnTypeConfirmation()) {
		return false;
	}
	$.post(
		"/ListManager/PutListColumnType",
        { "id": list, "column": columnName, "type": newType },
		success => {
			if (success) {
				hideLoadingIndicator();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

// Function for changing a column type //
function changeColumnDescription(list, columnName, description) {
    displayLoadingIndicator();
    $.post(
        "/ListManager/PutListColumnDescription",
        { "id": list, "column": columnName, "description": description },
        success => {
            if (success) {
                hideLoadingIndicator();
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
            }
        }
    ).fail(function (jqXHR, textStatus, errorThrown) {
        hideLoadingIndicator();
        displayErrorDialog();
    });
}

// Function for removing a column //
function removeColumn(list, columnName, tableRow) {
	displayLoadingIndicator();
	// Confirmation box for deleteing a column //
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/ListManager/RemoveColumn",
        { "id": list, "column": columnName },
		success => {
			if (success) {
				hideLoadingIndicator();
				tableRow.remove();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

// Function to get all lists //
function updateListBrowser(path, selector) {
	displayLoadingIndicator();
	$.get(
		"/ListManager/ListsContents",
		{},
		response => {
			$(selector).html(response);
			hideLoadingIndicator();
		}).fail(() => {
			hideLoadingIndicator();
			displayErrorDialog();
		});
}

// Function to delete a list //
function deleteList(listId, tableRow) {
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/ListManager/DeleteList",
		{ "id": listId },
		success => {
			if (success) {
				hideLoadingIndicator();
				tableRow.remove();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

// Function to add column //
function AddColumn(list) {
	var values = ["int", "string", "long string", "date", "double", "yes-no", "file", "image"];
	var text = ["Number", "Small Text", "Large Text", "Date", "Decimal", "Yes/No", "File", "Image"];
	var addHtml = '<tr>'
		+ '<td>'
        + '     <input type="text" class="column-name form-control" data-list-name="' + list + '" value="" />'
        + '</td>'
        + '<td>'
        + '     <input type="text" class="column-description form-control" value="" data-list-name="' + list + '" data-column-name="" />'
        + '</td>'
		+ '<td>'
        + '     <select class="column-type form-control" data-list-name="' + list + '">';
	for (var i = 0; i < values.length; i++) {
		addHtml += '<option value="' + values[i] + '">' + text[i] + '</option>';
	}
	addHtml += '</select>'
		+ '</td>'
		+ '<td>'
        + '     <button class="btn btn-danger delete-column" data-list-name="' + list + '">Delete</button>'
		+ '</td>'
		+ '</tr>';
	$("#column-table tbody").append(addHtml);
}

function displayDeleteConfirmation() {
	return confirm("Are you sure you'd like to delete this element?");
}

function displayColumnTypeConfirmation() {
	return confirm("Are you sure you'd like to change the column type since some data may be lost or changed?");
}

function displayLoadingIndicator() {
	console.log("Implement loading indicator.");
}

function hideLoadingIndicator() {
	console.log("Implement loading indicator hide.");
}

function displayErrorDialog() {
	console.log("Implement error dialog.");
}
