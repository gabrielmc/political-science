﻿/*
  ____   ____ _____   ____                          _
 / ___| / ___| ____| |  _ \ __ _  __ _  ___  ___   (_)___
 \___ \| |   |  _|   | |_) / _` |/ _` |/ _ \/ __|  | / __|
  ___) | |___| |___  |  __/ (_| | (_| |  __/\__ \_ | \__ \
 |____/ \____|_____| |_|   \__,_|\__, |\___||___(_)/ |___/
                                 |___/           |__/

Developed by the FHSS Web Team
BigText Generator: http://patorjk.com/software/taag/#p=display&f=Ivrit
*/
$(() => {
	$('#create-page-modal').on('hidden.bs.modal', () => {
		$("#create-page-modal").remove();
	});

	new Clipboard("[id^='file-path-trigger']");

	$("body").on("keyup", ".page-title-group input", function () {
		if (!window.HasOverriddenUriName) {
			if ($(this).val().trim() == "") {
				$(".page-uri-name-group input").val("");
				window.HasOverriddenUriName = false;
			} else {
				getUriNameSuggestion($(this).val().trim(), function (uriName) {
					$(".page-uri-name-group input").val(uriName);
					$(".page-uri-name-group input").change();
				});
			}
		}
	});

	$("body").on("keyup", ".page-uri-name-group input", function () { window.HasOverriddenUriName = $(this).val().trim() != "" });

	$("body").on("change", ".page-uri-name-group input", function () {
		var inputElem = $(this);
		var origName = inputElem.closest("form").attr("originaluri");
		console.log(origName);
		if ($(this).val().trim() == origName) {
			hideInvalidUriWarning();
		} else {
			displayUrinameValidation($(this).val().trim(), function () { hideInvalidUriWarning(inputElem) }, function () { showInvalidUri(inputElem) });
		}
	});
});

function deletePage(pageUriName) {
	return new Promise((resolve, reject) => {
		if (confirm("Are you sure you want to delete this page?\n\nDeleted pages cannot be recovered.")) {

			$.ajax({
				url: "/Admin/DeletePage/" + pageUriName,
				method: "POST",
				success: response => {
					$("#pages-area > tbody").html(response);
					resolve();
				},
				error: error => reject
			});
		}
	});
}

/* For the create-page-modal */

function newPageModal() {
	return new Promise((resolve, reject) => {
		if ($("#create-page-modal").length === 0) {
			$.get("/Admin/CreatePage").then(partialView => {
				$("body").append(partialView);
				$("#create-page-modal").modal("show");
				resolve();
			}).fail(error => reject);
		}
		else {
			$("#create-page-modal").modal("show");
			resolve();
		}
	});
}

function submitNewPage() {
	let data = $("#create-page-form").serializeArray();

	let jsondata = {};

	for (let input of data) jsondata[input.name] = input.value;

	console.log("jsondata:", jsondata);

	$.post("/Admin/CreatePage", jsondata).done(response => {
		location = response;
		$("#create-page-form")[0].reset();
	}).fail(err => console.error);
}

/* Helper functions for New and Edit Pages. */
function getUriNameSuggestion(uriName, callback) {
	$.get("/sce-api/PageManager/UriName/" + uriName.toString(),
		function (uriName) {
			callback(uriName);
		});
}

function displayUrinameValidation(uriName, callbackValid, callbackInvalid) {
	getUriNameSuggestion(uriName, function (response) {
		if (response == uriName) {
			callbackValid();
		} else {
			callbackInvalid();
		}
	});
}

function showInvalidUri(uriElem) {
	$(uriElem).siblings(".invalid-uri-warning").show();
}

function hideInvalidUriWarning(uriElem) {
	$(uriElem).siblings(".invalid-uri-warning").hide();
}