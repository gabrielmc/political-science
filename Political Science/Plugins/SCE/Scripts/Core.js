﻿/*
  ____   ____ _____    ____                  _
 / ___| / ___| ____|  / ___|___  _ __ ___   (_)___
 \___ \| |   |  _|   | |   / _ \| '__/ _ \  | / __|
  ___) | |___| |___  | |__| (_) | | |  __/_ | \__ \
 |____/ \____|_____|  \____\___/|_|  \___(_)/ |___/
                                          |__/

Developed by the FHSS Web Team
BigText Generator: http://patorjk.com/software/taag/#p=display&f=Ivrit
*/

// This is an object containing functions for each plugin, so that we don't have to
// change Core.js anymore. What needs to happen now, is that on any given Plugn.js,
// you will need to add the manager function as a key value pair of this object like this:

/*
	PluginFunctions["PluginName"] = function () {
		// code here
	};
*/
let PluginFunctions = {};
let PluginLoadFunctions = {};
let PluginCreateItemFunctions = {};
let PluginCreateCollectionFunctions = {};

// These are for saving dropdowns and Collections when necessary
let CollectionJson = {};
let DropDownContent = [];
let Collection;

$(() => {
	// Retrieve plugin name from the URL
	Collection = $("#collection").html();

	// Call the manager function for
	// that plugin if it exists
	if (typeof PluginFunctions[Collection] === 'function') PluginFunctions[Collection]();
	else $("#CoreEditor").html("<h1>This plugin does not exist, or is not installed.</h1><p>If you'd like this functionality to be included in your website, please contact us at <a href='mailto:fhss-web@byu.edu'>fhss-web@byu.edu</a></p>");

	//#region PageEditor
	{
		// Loads the tinymce text editor
		$("div[id^='content']").each(index => {
			console.log(index);
			tinymce.init({
				selector: "textarea#mytextareacontent" + index,
				plugins: 'advlist autolink link image media lists charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor',
				toolbar: 'undo redo | indent outdent | bold italic underline forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image media | file code',
				visualblocks_default_state: true,
				setup: editor => editor.on('change', tinymce.triggerSave),
				link_list: "/sce-api/PageManager/Links/Pages",
				file_picker_callback: filePickerCallback
			});
			$("#mceu_44-action").click();
		});
	}
	//#endregion

	// This script allows dropdown menus to work
	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$(this).parent().siblings().removeClass('open');
		$(this).parent().toggleClass('open');
	});

});

/* Content Editor Functions */

function loadTemplates(plugin) {
	return Promise.all([
		$.get("/Plugins/" + plugin + "/Templates/Editor/pill.html"),
		$.get("/Plugins/" + plugin + "/Templates/Editor/plus.html"),
		$.get("/Plugins/" + plugin + "/Templates/Editor/pane.html"),
		$.get("/Plugins/" + plugin + "/Templates/Editor/form.html")
	]);
}

function saveContent(form, id) {
	tinymce.triggerSave();

	let dataObj = {
		"page": $(form).serialize()
	};

	// use ajax to make a smooth update of on-page content
	$.ajax({
		url: "/sce-api/Content/Save/" + id,
		data: JSON.stringify(dataObj),
		method: "POST",
		dataType: "json",
		success: returnHTML => {
			console.log(returnHTML);
			if (returnHTML !== "ERROR") {
				//close modal and update contents of page
				$("#closeEditModal" + id).click();
				$("#content" + id).html(returnHTML);
			}
		},
		error: err => console.log
	});
}

function MenuChange() {
	switch ($("#select" + Collection).val()) {
		case 'new':
			$("#delete").prop("disabled", "disabled");
			$("#CreateNew" + Collection).slideDown(200);
			$("#EditorForm").slideUp(300, () => {
				PluginCreateCollectionFunctions[Collection]();
			});
			break;
		case 'Main':
			$("#delete").prop("disabled", "disabled");
			$("#CreateNew" + Collection).slideUp(200);
			$("#New" + Collection).val("");
			$("#EditorForm").slideUp(300, () => {
				LoadItems();
			}).delay(100).slideDown(300);
			break;
		default:
			$("#delete").prop("disabled", "");
			$("#CreateNew" + Collection).slideUp(200);
			$("#New" + Collection).val("");
			$("#EditorForm").slideUp(300, () => {
				LoadItems();
			}).delay(100).slideDown(300);
			break;
	}
}

function PillChange(id, thisid) {
	$(id).html($('#' + thisid).val());
}

function LoadItems() {
	if (typeof PluginLoadFunctions[Collection] === "function")
		PluginLoadFunctions[Collection]($("#select" + Collection).val(), CollectionJson[$("#select" + Collection).val()]);
}

function AddItem(id) {
	// Load templates
	PluginCreateItemFunctions[Collection](id);
}

function DeleteItem(id) {

	// Get the elements to delete
	const deadpill = $('#Pill' + id);
	const deadpane = $(deadpill.attr('aria-controls'));

	// If they're active, deactivate
	deadpill.removeClass('active');
	deadpane.removeClass('active');

	// Set active pane and pill to first in list,
	// or the next if we're deleting the first
	if (deadpill[0] !== $("#EditorPills > li").first()[0]) {
		$("#EditorPills > li").first().addClass('active');
		$($("#EditorPills > li").first().attr('aria-controls')).addClass('active');
	}
	else {
		$("#EditorPills > li").first().siblings().first().addClass('active');
		let PaneID = $("#EditorPills > li").first().siblings().first().attr('aria-controls');
		$(PaneID).addClass('active');
	}

	// Delete old items
	let removePills = new Promise(resolve => {
		deadpill.slideUp(400, () => {
			deadpill.remove();
			deadpane.remove();
			resolve();
		});
	});

	// Change the display order of all the following items
	removePills.then(() => {
		$("#EditorPills > li").each((index, element) => {
			let PaneID = $(element).attr('aria-controls');
			$(element).find("span.pill-order").html(parseInt(index + 1));
			$(PaneID).find('input[name="DisplayOrder"]').val(index);
		});
	});
}

/*
*	TinyMCE helper function
*/

let _MCECallback; // Holds the calback for the file_picker_callback TinyMCE option.
let _MCECallbackFile; // Holds the file (the first argument)
let _MVCCallbackObject; // Holds the object that it should return.

function tinyMCECallback(fileName) {
	_MCECallback(fileName);
}

function filePickerCallback(callback, value, meta) {
	_MCECallback = callback;
	$.get("/FileManager/FilePicker?Target=tinyMCECallback&FileType=" + meta.filetype.toString(),
		filePicker => {
			$("body").append(filePicker);
			$("#file-picker-modal").modal("show");
		}
	);
}
