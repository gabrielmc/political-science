﻿/*
  ____   ____ _____   _   _                      _
 / ___| / ___| ____| | | | |___  ___ _ __ ___   (_)___
 \___ \| |   |  _|   | | | / __|/ _ \ '__/ __|  | / __|
  ___) | |___| |___  | |_| \__ \  __/ |  \__ \_ | \__ \
 |____/ \____|_____|  \___/|___/\___|_|  |___(_)/ |___/
                                              |__/

Developed by the FHSS Web Team
BigText Generator: http://patorjk.com/software/taag/#p=display&f=Ivrit
*/
function searchName(divId, groupName) {
	var divSelector = "#" + divId;
	var netid = $(divSelector + " .user-netid").val();

	if (netid != "") {
		$(divSelector + " .results-panel").slideDown(100, () => {
			$(divSelector + " .spin-div").slideDown(100, () => {
				$(divSelector + " .spin-div div").each((index, element) => {
					$(element).fadeIn(600);
				});
				getJSON("/sce-api/ldap/SearchResults/" + netid)
					.then(json => {
						var i = 0;
						var firstfive = {};
						while (i < 5) {
							if (typeof json[Object.keys(json)[i]] !== 'undefined')
								firstfive[Object.keys(json)[i]] = json[Object.keys(json)[i]];
							i++;
						}
						return firstfive;
					})
					.then(five => {
						var pArr = [];
						$(divSelector + " .spin-div div").each((index, element) => {
							pArr.push(new Promise((resolve, reject) => $(element).fadeOut(() => resolve())));
						});
						Promise.all(pArr).then(() => {
							$(divSelector + " .spin-div").slideUp(100, () => {
								$(divSelector + " .netid-search-results-table").slideUp(() => {
									$(divSelector + " .netid-search-results-tbody").html("");
									for (var key in five) {
										var job = five[key].EmpClassification === null ? "" : five[key].EmpClassification;
										$(divSelector + " .netid-search-results-tbody").append("<tr><td>" + five[key].Name + "</td><td>" + key + "</td><td>" + job + "</td><td><button type='button' class='btn btn-success' onclick='addToGroup(\"" + key + "\", \"" + groupName + "\", \"" + divSelector + "\"); return false;'><span class='glyphicon glyphicon-plus'></span></button></td><tr>");
									}
									$(divSelector + " .netid-search-results-table").slideDown();
								});
							});
						});
					});
			});
		});
	}
	else {
		alert("Please input a NetID")
	}
}

function addToGroup(netid, groupName, replacementSelector) {
	return new Promise((resolve, reject) => {
		$.ajax({
			url: "/Admin/AddUser/" + groupName + "?NetID=" + netid + "&ContainerDiv=" + replacementSelector.substr(1),
			type: "POST",
			success: partialview => {
				$(replacementSelector + " .netid-search-results-table").slideUp(() => {
					$(replacementSelector + " netid-search-results-tbody").html("");
				});
				$(replacementSelector + " .user-list-table").slideUp(() => {
					$(replacementSelector + " .user-list-tbody").html(partialview);
					$(replacementSelector + " .user-list-table").slideDown(resolve);
				});
			},
			error: error => reject
		});
	});
}

function removeFromGroup(netid, groupName, replacementSelector) {
	return new Promise((resolve, reject) => {
		$.ajax({
			url: "/Admin/RemoveUser/" + groupName + "?NetID=" + netid + "&ContainerDiv=" + replacementSelector.substr(1),
			type: "POST",
			success: partialview => {
				$(replacementSelector + " .netid-search-results-table").slideUp(() => {
					$(replacementSelector + " netid-search-results-tbody").html("");
				});
				$(replacementSelector + " .user-list-table").slideUp(() => {
					$(replacementSelector + " .user-list-tbody").html(partialview);
					$(replacementSelector + " .user-list-table").slideDown(resolve);
				});
			},
			error: error => reject
		});
	});
}

function newGroup(inputSelector, groupsAreaSelector) {
	return new Promise((resolve, reject) => {
		var NewGroupName = $(inputSelector).val();
		$.ajax({
			url: "/Admin/AddGroup/" + NewGroupName,
			type: "POST",
			success: partialview => {
				var BsPartialview = $(partialview);
				BsPartialview.hide();
				$(groupsAreaSelector).append(BsPartialview);
				$(inputSelector).val("");
				BsPartialview.slideDown(resolve);
			},
			error: error => reject
		});
	});
}

function deleteGroup(groupName, groupContainerSelector) {
	if (confirm("Are you sure you would like to delete the group \"" + groupName + "\"?")) {
		return new Promise((resolve, reject) => {
			$.ajax({
				url: "/Admin/RemoveGroup/" + groupName,
				type: "POST",
				success: done => {
					var BsPanel = $(groupContainerSelector).parentsUntil("#accordion").first();
					BsPanel.slideUp(() => {
						BsPanel.remove();
						resolve();
					});
				},
				error: error => reject
			});
		});
	}
}

function getJSON(url) {
	return new Promise((resolve, reject) => {
		$.getJSON(url).then(JSON.parse).then(resolve).fail(reject);
	});
}