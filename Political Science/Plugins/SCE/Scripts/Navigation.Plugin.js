﻿/*
  _   _             _             _   _                _     
 | \ | | __ ___   _(_) __ _  __ _| |_(_) ___  _ __    (_)___ 
 |  \| |/ _` \ \ / / |/ _` |/ _` | __| |/ _ \| '_ \   | / __|
 | |\  | (_| |\ V /| | (_| | (_| | |_| | (_) | | | |_ | \__ \
 |_| \_|\__,_| \_/ |_|\__, |\__,_|\__|_|\___/|_| |_(_)/ |___/
                      |___/                         |__/     

Developed by the FHSS Web Team
BigText Generator: http://patorjk.com/software/taag/#p=display&f=Ivrit
*/

PluginFunctions["Navigation"] = function () {

	let LoadAll = [
		$.getJSON("/Plugins/SCE/Content/Navigation/Navigation.json"),
		$.get("/Plugins/SCE/Templates/Editor/wrap.html")
	];

	Promise.all(LoadAll).then(results => {
		CollectionJson = results[0];
		let wrap = results[1];

		$("#CoreEditor").html(wrap.replaceAll("{{REPLACE_ITEM_TYPE}}", "Navigation"));

		// Check if Main exists (if not, copy the following into Navigation.json)
		/*
		{
			"Main": [
				{
					"ID": 0,
					"Title": "Home",
					"MenuType": "Link",
					"Link": "/",
					"DropDownId": -1,
					"DisplayOrder": 1
				}
			]
		}
		*/

		if (typeof CollectionJson.Main !== 'undefined') {

			// Add items to select dropdown
			for (let Navigation in CollectionJson)
				$("#select" + Collection).append('<option value="' + Navigation + '">' + Navigation + '</option>');

			// Add the 'create new' option
			$("#select" + Collection).append('<option value="new">Create New Navigation</option>');

			// Display the Main Navigation
			//LoadNavigationItems("Main", CollectionJson.Main);
			PluginLoadFunctions["Navigation"]("Main", CollectionJson["Main"]);
		}
		else {
			alert("ERROR: The 'Main' Navigation seems to be missing.\n\nEmail us at fhss-web@byu.edu and let us know.");
			location.replace('/');
		}

		// Enables drag and drop from jquery-ui
		$(".sortable").sortable({

			// This defines what happens when the stack is re-ordered
			update: (event, uiObject) => {

				// Loop through the current li objects in the div
				$("#EditorPills > li").each((index, element) => {
					let PaneID = $(element).attr('aria-controls');
					let NewDO = index;
					// Change the pill-order span
					$(element).find('span.pill-order').html(NewDO + 1);
					// Change the display order input on the form
					$(PaneID).find('input[name="DisplayOrder"]').val(NewDO);
				});

			},

			// This enforces that the items can only be moved on the y axis
			axis: "y",

			// This adds a class to the placeholder
			placeholder: "PillPlaceholder",

			// This forces the size to stay
			forcePlaceholderSize: true,

			// This only allows li items to be sortable
			items: '> li'
		});
	});

	// Event handlers
	$("#CoreEditor").on("change", ".menu-type-select", function () {
		let MenuType = $(this).val();
		switch (MenuType) {
			case "DropDown":
				$(this).closest(".tab-pane").find(".menu-link-group").slideUp();
				$(this).closest(".tab-pane").find(".menu-dropdown-group").slideDown();
				break;
			default:
				$(this).closest(".tab-pane").find(".menu-link-group").slideDown();
				$(this).closest(".tab-pane").find(".menu-dropdown-group").slideUp();
		}
	});

	$(".menu-type-select").change();
};

PluginLoadFunctions["Navigation"] = function (Key, Navigation) {

	let Templates = {};
	loadTemplates("SCE").then(results => {
		Templates.Pill = results[0];
		Templates.Plus = results[1];
		Templates.Pane = results[2];
		Templates.Form = results[3];

		// sorts navigation by display order
		Navigation = Navigation.sort(function (a, b) { return a.DisplayOrder > b.DisplayOrder; });

		// Clears current pills
		$("#EditorPills").html("");

		for (let Item in Navigation) {
			// Add each pill
			$("#EditorPills").append(Templates.Pill
				.replaceAll("{{REPLACE_DISPLAYORDER}}", Item)
				.replaceAll("{{REPLACE_TITLE}}", Navigation[Item].Title)
				.replaceAll("{{REPLACE_REAL_DISPLAYORDER}}", Navigation[Item].DisplayOrder + 1)
			);
		}

		// Adds the '+' pill
		$("#AddNavBtnOuter").remove();
		$("#EditorPills").after(Templates.Plus
			.replaceAll("{{REPLACE_NEW_ID}}", Navigation.length)
		);

		// Clears current panes
		$("#EditorPanes").html("");

		// Create array of promises to fulfill before continuing
		let PromiseArr = [];

		for (const Item in Navigation) {
			// If there's a dropdown, 
			if (Navigation[Item].DropDownId !== -1) {
				// Add a promise to the promise array to load it
				PromiseArr.push(
					new Promise((resolve, reject) => {
						$.get("/Plugins/SCE/Content/Navigation/DropDowns/" + Key + "_DD" + Navigation[Item].DropDownId + ".html").then(dd => {
							let pane = Templates.Pane
								.replaceAll("{{REPLACE_DISPLAYORDER}}", Item)
								.replaceAll("{{REPLACE_FORM}}", Templates.Form
									.replaceAll("{{REPLACE_DISPLAYORDER}}", Item)
									.replaceAll("{{REPLACE_TITLE}}", Navigation[Item].Title)
									.replaceAll("{{REPLACE_LINK}}", Navigation[Item].Link)
									.replaceAll("{{REPLACE_DD}}", dd)
									.replaceAll("{{REPLACE_DISPLAYORDER}}", Navigation[Item].DisplayOrder)
								);

							switch (Navigation[Item].MenuType) {
								case "Link":
									pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "selected")
										.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
										.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "");
									break;
								case "Link to New Window":
									pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "")
										.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "selected")
										.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "");
									break;
								case "DropDown":
									pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "")
										.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
										.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "selected");
									break;
								default:
									break;
							}
							$("#EditorPanes").append(pane);
							resolve();
						});
					})
				);
			}
			else {
				let pane = Templates.Pane
					.replaceAll("{{REPLACE_DISPLAYORDER}}", Item)
					.replaceAll("{{REPLACE_FORM}}", Templates.Form
						.replaceAll("{{REPLACE_DISPLAYORDER}}", Item)
						.replaceAll("{{REPLACE_TITLE}}", Navigation[Item].Title)
						.replaceAll("{{REPLACE_LINK}}", Navigation[Item].Link)
						.replaceAll("{{REPLACE_DD}}", "")
						.replaceAll("{{REPLACE_DISPLAYORDER}}", Navigation[Item].DisplayOrder)
					);

				switch (Navigation[Item].MenuType) {
					case "Link":
						pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "selected")
							.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
							.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "");
						break;
					case "Link to New Window":
						pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "")
							.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "selected")
							.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "");
						break;
					case "DropDown":
						pane = pane.replaceAll("{{REPLACE_SEL_LINK}}", "")
							.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
							.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "selected");
						break;
					default:
						break;
				}
				$("#EditorPanes").append(pane);
			}
		}

		Promise.all(PromiseArr).then(results => {
			// Make the first one active
			$("#EditorPills > li").first().addClass('active');
			$($("#EditorPills > li").first().attr('aria-controls')).addClass('active');

			// Shows all elements
			$("#EditorPills > li").each((index, element) => {
				$(element).show();
			});

			$(".menu-type-select").change();
		});
	});
};

PluginCreateCollectionFunctions["Navigation"] = function () {

	let Templates = {};
	loadTemplates("SCE").then(results => {
		Templates.Pill = results[0];
		Templates.Plus = results[1];
		Templates.Pane = results[2];
		Templates.Form = results[3];

		// Clear the current pills
		$("#EditorPills").html("");

		// Add the first Pill
		$("#EditorPills").append(Templates.Pill
			.replaceAll("{{REPLACE_DISPLAYORDER}}", 0)
			.replaceAll("{{REPLACE_TITLE}}", "New Item")
			.replaceAll("{{REPLACE_REAL_DISPLAYORDER}}", 1)
		);

		// Clear the current panes
		$("#EditorPanes").html("");

		// Add the first Pane
		$("#EditorPanes").append(Templates.Pane
			.replaceAll("{{REPLACE_DISPLAYORDER}}", 0)
			.replaceAll("{{REPLACE_FORM}}", Templates.Form
				.replaceAll("{{REPLACE_DISPLAYORDER}}", 0)
				.replaceAll("{{REPLACE_SEL_LINK}}", "selected")
				.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
				.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "")
				.replaceAll("{{REPLACE_TITLE}}", "New Item")
				.replaceAll("{{REPLACE_LINK}}", "")
				.replaceAll("{{REPLACE_DD}}", "")
				.replaceAll("{{REPLACE_DISPLAYORDER}}", 1)
			));

		$("#AddNavBtn").attr("onclick", "AddItem(1); return false;");

		// Show the pill and pane
		$("#EditorPills > li").each((index, element) => {
			$(element).show().addClass('active');
			let PaneID = $(element).attr('aria-controls');
			$(PaneID).addClass('active');
		});

		$("#EditorForm").slideDown(300);
	});
};

PluginCreateItemFunctions["Navigation"] = function (id) {
	let Templates = {};

	// This does the processing at the same time, multithreading!
	// now we can wait until all the gets are done before moving on,
	// and they all run asynchronously!

	loadTemplates("SCE").then(results => {
		Templates.Pill = results[0];
		Templates.Pane = results[2];
		Templates.Form = results[3];

		// Get the new id from the length of the current list
		let displayorder = parseInt($("#EditorPills > li").length);

		// Add pill
		$("#EditorPills").append(Templates.Pill
			.replaceAll("{{REPLACE_DISPLAYORDER}}", id)
			.replaceAll("{{REPLACE_TITLE}}", "New Item")
			.replaceAll("{{REPLACE_REAL_DISPLAYORDER}}", displayorder + 1)
		);

		$("#Pill" + id).slideDown();

		// Add pane
		$("#EditorPanes").append(Templates.Pane
			.replaceAll("{{REPLACE_DISPLAYORDER}}", id)
			.replaceAll("{{REPLACE_FORM}}", Templates.Form
				.replaceAll("{{REPLACE_DISPLAYORDER}}", id)
				.replaceAll("{{REPLACE_SEL_LINK}}", "selected")
				.replaceAll("{{REPLACE_NEWWINDOW_SEL}}", "")
				.replaceAll("{{REPLACE_DROPDOWN_SEL}}", "")
				.replaceAll("{{REPLACE_TITLE}}", "New Item")
				.replaceAll("{{REPLACE_LINK}}", "")
				.replaceAll("{{REPLACE_DD}}", "")
				.replaceAll("{{REPLACE_DISPLAYORDER}}", displayorder)
			));

		// Change '+' pill
		$("#AddNavBtn").attr('onclick', 'AddItem(' + (id + 1) + '); return false;');
	});
};

function DeleteNavigation() {
	const Navigation = $("#selectNavigation").val();
	if (Navigation !== "Main" && Navigation !== "new") {
		delete CollectionJson[Navigation];
		$.ajax({
			url: "/sce-api/DropDown/Delete/" + Navigation,
			type: "DELETE",
			success: function (result) {
				console.log("Deleted all DropDowns");
				let CollectionType = $("#selectNavigation").val();
				$.ajax({
					url: "/sce-api/Navigation/Save/5",
					type: "POST",
					data: JSON.stringify(CollectionJson),
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					success: (message) => {
						console.log(message);
						console.log("Navigation saved.");
						console.log("Saving DropDowns...");
						$.ajax({
							url: "/sce-api/DropDown/Delete/" + CollectionType,
							type: "DELETE",
							success: function (result) {
								console.log("Deleted all DropDowns. Creating new ones...");
								$.ajax({
									url: "/sce-api/DropDown/Save/5",
									type: "POST",
									data: JSON.stringify(DropDownContent),
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function (result) {
										console.log(result);
										alert("Navigation saved.");
										location.reload();
									},
									error: function (err) {
										console.log(err);
										alert("Error Saving DropDowns");
									}
								});
							},
							error: function (err) {
								console.log(err);
								alert("Error Deleting DropDowns");
							}
						});
					},
					error: err => {
						console.log(err);
						alert("Error Posting Navigation");
					}
				});
			},
			error: function (err) {
				console.log(err);
				alert("Error Deleting DropDowns");
			}
		});
	}
}

function SaveNavigation() {

	// Get the form as a json object
	let CurrentFormObject = $("#ItemForm").serializeArray();

	// Get the name of the object from the select menu. If creating a new Navigation, change the Key.
	let Key = $("#select" + Collection).val() === "new" ? $("#NewNavigation").val() : $("#select" + Collection).val();

	if (Key !== "") {
		CollectionJson[Key] = [];

		// Get the number of items in the object
		let NumNavigationItems = 0;
		let NumFormInputs = 5;

		NumNavigationItems = CurrentFormObject.length / NumFormInputs;

		for (let i = 0; i < NumNavigationItems; i++) {
			CollectionJson[Key][i] = {
				"DisplayOrder": null,
				"Link": null,
				"MenuType": null,
				"DropDownId": null,
				"Title": null
			};
		}

		// Create an Array for storing the dropdowns if we're editing Navigation, in order to save them seperately
		// We seperate the DropDown content from the navigation JSON in order to keep the nav JSON clean
		let DropDownId = 0;

		// Loop through each item, and each input in each form
		for (let i = 0; i < NumNavigationItems; i++) {
			for (let j = 0; j < NumFormInputs; j++) {

				// This will keep track of which input we're on in the item
				let FormIndex = i * NumFormInputs + j;

				// If we're editing Navigation and we come across a DropDown, we must add it to the array
				if (CurrentFormObject[FormIndex].name === "DropDown") {

					// Make sure it's not empty
					if (CurrentFormObject[FormIndex].value !== "") {

						// Save the DropDown Content and iterate the DropDownId
						DropDownContent[DropDownId] = CurrentFormObject[FormIndex].value;
						CollectionJson[Key][i].DropDownId = DropDownId;
						DropDownId++;
					}

					// If the DropDown is empty, set that item's DropDownId to -1
					else CollectionJson[Key][i].DropDownId = -1;
				}

				// Set every item to it's corresponding value in the form input
				else CollectionJson[Key][i][CurrentFormObject[FormIndex].name] = CurrentFormObject[FormIndex].value;
			}
		}

		// We need to turn the dropdowns into C# DropDown models
		for (let i = 0; i < DropDownContent.length; i++) {
			DropDownContent[i] = {
				"Menu": $("#select" + Collection).val(),
				"DropDownId": i,
				"Html": DropDownContent[i]
			};
		}

		let CollectionType = $("#selectNavigation").val();

		if (confirm("Are you sure you want to save?\n\nThis will overwrite your previous " + Collection + ".")) {

			// Our json object is now ready to save, and our DropDown as well, if applicable
			$.ajax({
				url: "/sce-api/Navigation/Save/5",
				type: "POST",
				data: JSON.stringify(CollectionJson),
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				success: (message) => {
					console.log(message);
					console.log("Navigation saved.");

					console.log("Saving DropDowns...");
					$.ajax({
						url: "/sce-api/DropDown/Delete/" + CollectionType,
						type: "DELETE",
						success: function (result) {

							console.log("Deleted all DropDowns. Creating new ones...");

							$.ajax({
								url: "/sce-api/DropDown/Save/5",
								type: "POST",
								data: JSON.stringify(DropDownContent),
								dataType: "json",
								contentType: "application/json; charset=utf-8",
								success: function (result) {
									console.log(result);
									alert("Navigation saved.");
									location.reload();
								},
								error: function (err) {
									console.log(err);
									alert("Error Saving DropDowns");
								}
							});
						},
						error: function (err) {
							console.log(err);
							alert("Error Deleting DropDowns");
						}
					});
				},
				error: err => {
					console.log(err);
					alert("Error Posting Navigation");
				}
			});
		}
		else {
			console.log(CollectionJson);
		}
	}
	else {
		alert("Please give a name to your new Navigation.");
	}
}
