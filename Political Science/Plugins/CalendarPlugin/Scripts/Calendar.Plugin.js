﻿var calendarBrowserSelector = ".calendar-browser";
var calendarColumnSelector = ".calendar-columns";
var calendarItemsSelector = ".calendar-items";
var calendarPermissionsSelector = ".calendar-permissions";
var calendarNameSelector = ".main-controls";
var calendarName = $(".calendar-name").val();
var calendarView = "#calendar";
var calendarModalSelector = "#eventModal";
var table = $("#calendar-items-table");
$(document).ready(() => {
    var calendarName = $("#calendar").data("calendar-name");
    

    updateCalendarBrowser(window.location.hash.substr(1), calendarBrowserSelector);

    $(window).on("hashchange", () => {
        updateBrowser(window.location.hash.substr(1), calendarBrowserSelector);
    });

    // Delete list event function //
    $(calendarBrowserSelector).on("click", ".delete-calendar", e => {
        var tableRow = $(e.target).closest('tr');
        var filePath = $(e.target).data("listid");
        deleteCalendar(filePath, tableRow);
    });

    // Create list event function //
    $(calendarBrowserSelector).on("click", ".calendar-create-input", e => {
        createCalendar();
    });

    
    // Delete list item event function //
    $(calendarNameSelector).on("click", ".add-event", e => {
        var calendar = $('.calendar-name').html();
        addEvent(calendar);
    });
    
    // Edit list name event function //
    $(calendarNameSelector).on("focus", ".edit-calendar-name", e => {
        this.currentVal = $(e.target).val();
    }).on("change", ".edit-calendar-name", e => {
        var newVal = $(e.target).val();
        editCalendarName(currentVal, newVal);
        });

    // Edit list item value event function //
    $(calendarItemsSelector).on("change", ".field-value", e => {
        var list = $('.calendar-name').html();
        var id = $(e.target).data("list-item-id");
        var newVal = $(e.target).val();
        if ($(e.target).attr("type") == "checkbox") {
            if ($(e.target).is(":checked")) {
                newVal = true;
                $(".field-value[data-field-name='end']").attr("disabled", "disabled");
                $(".field-value[data-field-name='end']").val(null);
                saveEventValue(list, id, "end", null);
            }
            else {
                newVal = false;
                $(".field-value[data-field-name='end']").attr("disabled", false);
                var newDate = new Date($(".field-value[data-field-name='start']").val())
                newDate.setHours(newDate.getHours() + 1);
                function resolve(n) {
                    if (n < 10) { n = "0" + n }
                    return n;
                }
                var dateStr = newDate.getUTCFullYear() + "-" + resolve(newDate.getMonth()+1) + "-" + resolve(newDate.getDate()) + "T" + resolve(newDate.getHours()) + ":" + resolve(newDate.getMinutes());
                $(".field-value[data-field-name='end']").val(dateStr);
                saveEventValue(list, id, "end", dateStr);
            }
        }
        var column = $(e.target).data("field-name");
        saveEventValue(list, id, column, newVal);
    });

    // List permissions edit event //
    $(calendarPermissionsSelector).on("change", ".permission-value", e => {
        var permission = $(e.target).data("permission-name");
        var newVal = $(e.target).val();
        if ($(e.target).attr("type") == "checkbox") {
            if ($(e.target).is(":checked")) {
                newVal = true;
            }
            else {
                newVal = false;
            }
        }
        saveCalendarPermissions(permission, newVal);
    });

    // Delete list item event function //
    $(calendarModalSelector).on("click", ".event-edit", e => {
        var tableRow = $(e.target);
        var id = $(e.target).data("event-id");
        var calendar = $('.calendar-name').html();
        window.location.href = "/CalendarPlugin/EditEvent/" + calendar + "?id=" + id;
    });

    $(calendarItemsSelector).on("click", ".save-event", e => {
        alert("Your changes have been saved.");
    });

    if ($(calendarView)[0]) {
        var script = document.createElement("script");
        var script2 = document.createElement("script");
        var script3 = document.createElement("link");
        script.src = "/Plugins/CalendarPlugin/fullcalendar/lib/moment.min.js";
        script.type = "text/javascript";
        script2.src = "/Plugins/CalendarPlugin/fullcalendar/fullcalendar.js";
        script2.type = "text/javascript";
        script3.href = "/Plugins/CalendarPlugin/fullcalendar/fullcalendar.min.css";
        script3.rel = "stylesheet";
        document.body.append(script);
        document.body.append(script2);
        document.head.append(script3);
        if (script.readyState) {
            script.onreadystatechange = function () {
                if ((script.readyState == "loaded" || script.readyState == "complete") && (script2.readyState == "loaded" || script2.readyState == "complete") && (script3.readyState == "loaded" || script3.readyState == "complete")) {
                    script.onreadystatechange == null;
                    var getCalName = $(calendarView).data("calendar-name");
                    var calView = $(calendarView).data("calendar-view");
                    calendarEvents(getCalName, calView);
                }
            }
        }
        else {
            script2.onload = function () {
                var getCalName = $(calendarView).data("calendar-name");
                var calView = $(calendarView).data("calendar-view");
                calendarEvents(getCalName, calView);
            }
        }
    }
});

function upcomingEvents(data) {
    $("#calendar").append("<h3>Upcoming Events</h3><hr/>");
    var now = new Date();
    var sortedEvents = data.Rows.filter(a => new Date(a.start) > now);
    console.log(sortedEvents);
    sortedEvents = sortedEvents.sort(function (a, b) {
        //var aStart = new Date(a.start);
        //var bEnd = new Date(b.end);
        //    if (aStart > bEnd) {
        //        return 1;
        //    }
        //    if (aStart < bEnd) {
        //        return - 1;
        //    }
        //    return 0;

        var aStart = new Date(a.start);
        var bStart = new Date(b.start);
            if (aStart > bStart) {
                return 1;
            }
            if (aStart < bStart) {
                return - 1;
            }
            return 0;


    });
    var htmlStr = "";
    // Will display next 5 events
    for (var i = 0; i < 5 ; i++) {
        var eventDateStart = new Date(sortedEvents[i].start);
        var eventDateStartStr = "";
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        htmlStr += "<div class='calendarEvent'>"
            + " <h4>" + sortedEvents[i].title + "</h4>"
            + " <h5>" + dayNames[eventDateStart.getDay()] + ", " + monthNames[eventDateStart.getMonth()] + " " + eventDateStart.getDate() + nth(eventDateStart.getDate()) + "</h5>"
            + "<p>" + sortedEvents[i].description + "</p>"
            + "</div>";
    }
    $("#calendar").append(htmlStr);
    $(".calendarEvent").css("padding", "10px");
}

function nth(d) {
    if (d > 3 && d < 21) return "th";
    switch (d % 10) {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
        default: return "th";
    }
}

function resolve(n) {
    if (n < 10) { n = "0" + n }
    return n;
}

function convertToString(date) {
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    return dayNames[date.getDay()] + ", " + monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + " @ " + (date.getHours() > 12 ? resolve(date.getHours() % 12) : resolve(date.getHours())) + ":" + resolve(date.getMinutes()) + " " + (date.getHours() > 11 ? "PM" : "AM");
}

function initCalendar(data) {
    $("#calendar").fullCalendar({
        header: {
            left: 'prev,next,today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,list'
        },
        ignoreTimezone: false,
        eventSources: [
            data.Rows
        ],
        eventClick: function (calEvent, jsEvent, view) {

            $(".event-edit").attr("data-event-id", calEvent.ID);
            $(".event-delete").attr("data-event-id", calEvent.ID);
            $(".title").html(calEvent.title);
            $(".start").html(convertToString(new Date(calEvent.start._i)));
            if (calEvent.end != null) {
                $(".end").html(convertToString(new Date(calEvent.end._i)));
            }
            else {
                $(".end").html("N/A");
            }
            $(".allDay").html(calEvent.allDay);
            $(".description").html(calEvent.description);
            $(".url").html("<a href='calEvent.url'>" + calEvent.url + "</a>");
            var element = $(this);
            $(".event-delete").click(function () {
                deleteEvent($(".calendar-name").html(), calEvent.ID, element);
            });
            $("#eventModal").modal("show");
        },
        eventRender: function (event, element) {
            $(element).parent().find('a').prevObject.prevObject.prop("href", "#");
        },
        eventMouseover: function (calEvent, jsEvent, view) {
            $(this).css("cursor", "pointer");
        },
        views: {
            list: {
                duration: { days: 90 },
                listDayAltFormat: 'dddd'
            }
        }
    });
}

// Function for service to call a list //
function calendarEvents(calName, callback) {
    console.log(calName);
    $.get(
        "/CalendarPlugin/GetCalendarEvents/" + calName,
        success => {
            if (success) {
                var data = JSON.parse(success);
                hideLoadingIndicator();
                console.log(data);
                window[callback](data);
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
                console.log(success);
            }
        }
    )
        .fail(function (jqXHR, textStatus, errorThrown) {
            hideLoadingIndicator();
            displayErrorDialog();
        });
}

// Function for service to call a list //
function calendarService(calendar, callback, context) {
    $.get(
        "/CalendarPlugin/GetCalendar/" + calendar,
        success => {
            if (success) {
                var data = JSON.parse(success);
                hideLoadingIndicator();
                console.log(data);
                window[callback](context, data);
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
                console.log(success);
            }
        }
    )
        .fail(function (jqXHR, textStatus, errorThrown) {
            hideLoadingIndicator();
            displayErrorDialog();
        });
}

// Function to create a list //
function createCalendar() {
	// Prompt for name of list //
	var calendarName = prompt("What is the name of your new list?");
    if (calendarName != null) {
		$.post(
			"/CalendarPlugin/AddCalendar",
            { "id": calendarName },
			success => {
				if (success) {
					hideLoadingIndicator();
					// Load the edit list form for the new list created //
                    window.location.href = "/CalendarPlugin/EditCalendar/" + calendarName;
				} else {
					hideLoadingIndicator();
					displayErrorDialog();
				}
			}
		).fail(function (jqXHR, textStatus, errorThrown) {
			hideLoadingIndicator();
			displayErrorDialog();
		});
    }
    else {
        alert("Error: Please enter a valid calendar name!");
    }
}

// Function to edit list permissions //
function saveCalendarPermissions(permission, value) {
    $.post(
        "/CalendarPlugin/EditCalendarPermissions",
        { "calendar": listNameSelector, "permission": permission, "value": value },
        success => {
            if (success) {
                hideLoadingIndicator();
            } else {
                hideLoadingIndicator();
                displayErrorDialog();
            }
        }
    ).fail(function (jqXHR, textStatus, errorThrown) {
        hideLoadingIndicator();
        displayErrorDialog();
    });
}

// Function to save a list item //
function saveEventValue(list, id, column, value) {
	$.post(
		"/CalendarPlugin/SaveEvent",
		{ "calendar": list, "id": id, "field": column, "value": value },
		success => {
			if (success) {
				hideLoadingIndicator();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

// Function to delete a list item //
function deleteEvent(calendar, id, element) {
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/CalendarPlugin/DeleteEvent",
        { "calendar": calendar, "id": id },
		success => {
			if (success) {
                hideLoadingIndicator();
                $(element).remove();
                $("#eventModal").modal("hide");
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function to add a list item //
function addEvent(calendar) {
	$.post(
		"/CalendarPlugin/AddEventID",
        { "calendar": calendar },
		success => {
			console.log(success);
			if (success > -1) {
				hideLoadingIndicator();
                window.location.href = "/CalendarPlugin/AddEvent/" + calendar + "?id=" + success;
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function for editing list name //
function editCalendarName(currentVal, newVal) {
	$.post(
		"/CalendarPlugin/EditCalendarName",
		{ "oldName": currentVal, "newName": newVal },
		success => {
			if (success) {
				hideLoadingIndicator();
				window.location.href = "/CalendarPlugin/EditCalendar/" + newVal;
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();

	});
}

// Function to get all lists //
function updateCalendarBrowser(path, selector) {
	displayLoadingIndicator();
	$.get(
		"/CalendarPlugin/CalendarContents",
		{},
		response => {
			$(selector).html(response);
			hideLoadingIndicator();
		}).fail(() => {
			hideLoadingIndicator();
			displayErrorDialog();
		});
}

// Function to delete a list //
function deleteList(listId, tableRow) {
	if (!displayDeleteConfirmation()) {
		return false;
	}
	$.post(
		"/ListManager/DeleteList",
		{ "id": listId },
		success => {
			if (success) {
				hideLoadingIndicator();
				tableRow.remove();
			} else {
				hideLoadingIndicator();
				displayErrorDialog();
			}
		}
	).fail(function (jqXHR, textStatus, errorThrown) {
		hideLoadingIndicator();
		displayErrorDialog();
	});
}

function displayDeleteConfirmation() {
	return confirm("Are you sure you'd like to delete this element?");
}

function displayColumnTypeConfirmation() {
	return confirm("Are you sure you'd like to change the column type since some data may be lost or changed?");
}

function displayLoadingIndicator() {
	console.log("Implement loading indicator.");
}

function hideLoadingIndicator() {
	console.log("Implement loading indicator hide.");
}

function displayErrorDialog() {
	console.log("Implement error dialog.");
}
