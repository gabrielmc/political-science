﻿using Byu.Fhss.Sce.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Political_Science.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [SceAuthorize]
        public FileResult DepartmentHandbook()
        {
            ViewBag.Message = "Your contact page.";
            
            return File("/Plugins/FileManager/Files/Faculty/DeptHandbookAug09.pdf", "application/pdf");
        }

        [SceAuthorize]
        public FileResult InternalDirectory()
        {
            ViewBag.Message = "Your contact page.";

            return File("/Plugins/FileManager/Files/Faculty/InternalFacultyContactList.pdf", "application/pdf");
        }
    }
}